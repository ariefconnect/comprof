<!DOCTYPE html>
<html>
<head>
  <title>Company Profil</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="./bootstrap-3.3.6/dist/css/bootstrap.min.css">
  <script src="./bootstrap-3.3.6/dist/js/bootstrap.min.js"></script>
  <script src="./bootstrap-3.3.6/dist/js/jquery.min.js"></script>
  <script src="./bootstrap-3.3.6/dist/css/bootstrap-theme.min.js"></script>
  <!--<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="./custom/custom.css">
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
  <header>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand text-image" href="#" style="font-family:arial;">Keremuting.Net</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="#home">Home</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portofolio">Portofolio</a></li>
          <li><a href="#about">About</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
        </div>
      </div>
    </div>
    </nav>
  </header>
  <aside>
  <div class="col-md-13" id="home">
    <div class="jumbotron text-center home">
      <h1>Do What Do You Want To Do</h1>
      <p>We Specialize In </p>

      <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <h4>"WEBSITE APPLICATION"<br><span style="font-style:normal;">Apriadi, Arief, Arie, & By BJNICE</span></h4>
          </div>
          <div class="item">
            <h4>"DEKSTOP APPLICATION"<br><span style="font-style:normal;">Apriadi, Arief, Arie, & By BJNICE</span></h4>
          </div>
          <div class="item">
            <h4>"ANDROID APPLICATION"<br><span style="font-style:normal;">Apriadi, Arief, Arie, & By BJNICE</span></h4>
          </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
  <div class="container" id="about">
    <div class="col-md-12 about">
      <div class="col-md-7 ">
        <img src="./img/gray-logo.png" width="450" height="450">
      </div>
      <div class="col-md-5">
        <h2>Keremunting.Net</h2>
        <hr>
        <p align="justify">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </p>
      </div>
    </div>
  </div>
  <div class="container" id="services">
    <div class="col-md-13 about">
      <h2 align="center">Our Services</h2>
      <hr>
      <div class="col-md-4" align="center">
        <img src="img/gray-logo.png" align="center" width="120" height="120">
        <h3 align="center">
          Website Apps
        </h3>
        <p align="center" style="padding:20px 20px 20px 20px;">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
        </p>
      </div>
      <div class="col-md-4" align="center">
        <img src="img/gray-logo.png" align="center" width="120" height="120">
        <h3 align="center">
          Dekstop Apps
        </h3>
        <p align="center" style="padding:20px 20px 20px 20px;">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
        </p>
      </div>
      <div class="col-md-4" align="center">
        <img src="img/gray-logo.png" align="center" width="120" height="120">
        <h3 align="center">
          Android Apps
        </h3>
        <p align="center" style="padding:20px 20px 20px 20px;">
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
        </p>
      </div>
    </div>
    <div class="col-md-13 about" id="portofolio">
      <h2 align="center">Our Products</h2>
      <hr>
    </div>
    <div class="col-md-13 contact" id="contact">
      <h2 align="center">Contact Us</h2>
      <hr>
      <div class="col-md-8">
        <h3 align="center">Want to Talk?</h3>
        <div class="col-md-12">
          <form action="#" method="post" enctype="multipart/form-data" role="form">
            <div class="form-group">
              <div class="col-xs-6">
                <input type="text" class="form-control" name="nama" placeholder="Your Name">
              </div>
              <div class="col-xs-6">
                <input type="email" class="form-control" name="email" placeholder="Your Email">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                <input type="text" class="form-control" name="email" placeholder="Subject">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                <textarea name="message" placeholder="I Want to Talk About .." class="form-control" rows="5"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12" align="right">
                <input type="submit" name="btn_submit" value="Send Message" class="btn btn-primary btn-md">
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-4">
        <h3>Location</h3>
        <div id="googleMap" style="height:300px;width:100%;"></div>

<!-- Add Google Maps -->
      <script src="http://maps.googleapis.com/maps/api/js"></script>
      <script>
      var myCenter = new google.maps.LatLng(107.6416459, -6.898964);

      function initialize() {
      var mapProp = {
      center:myCenter,
      zoom:12,
      scrollwheel:false,
      draggable:false,
      mapTypeId:google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

      var marker = new google.maps.Marker({
      position:myCenter,
      });

      marker.setMap(map);
      }

      google.maps.event.addDomListener(window, 'load', initialize);
      </script>

      </div>
    </div>
    <hr>
  </div>
  </aside>
  <footer>
    <div class="container" style="margin-bottom:10px;">
      <div class="col-md-10">
        <p style="padding:0; margin:0;"><img src="./img/gray-logo.png" width="20" height="20"> Jalan cikutra nomor sekian</p>
        <p style="padding:0; margin:0;"><img src="./img/gray-logo.png" width="20" height="20">  Bandung, 40124</p>
        <p style="padding:0; margin:0;"><img src="./img/gray-logo.png" width="20" height="20">  Indonesia</p>
        <p style="padding:0; margin:0;"><img src="./img/gray-logo.png" width="20" height="20">  621-8212371238</p>
        <p style="padding:0; margin:0;"><img src="./img/gray-logo.png" width="20" height="20">  info@keremunting.net</p>
        <p style="padding:0; margin:0;"><img src="./img/gray-logo.png" width="20" height="20">  www.keremunting.net</p>
      </div>
      <div class="col-md-2" align="right">
          <p>
            <a href="#myPage" title="To Top">
              <span class="glyphicon glyphicon-chevron-up" style="color:#e74c3c;"></span>
            </a>
            Copyright By BJNICE. <a href="keremunting.net">Keremunting Team</a></p>
      </div>
    </div>
  </footer>

  <script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 900, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
  });

  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
</script>
</body>
</html>
